CXX = g++
CXXFLAGS = -g -Wall -Wextra

.PHONY: all
all : Menu
Menu: Menu.cpp Item.o
	$(CXX) $(CXXFLAGS) -o $@ $^
Item.o: Item.cpp  Item.h
	$(CXX) $(CXXFLAGS) -c $<

.PHONY : clean
clean:
	rm *~ *.o Menu

