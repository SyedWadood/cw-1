/*
Menu.cpp
Author: M00734132
Created: 12/01/2021
Updated: 22/01/2021
*/

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include<iterator>

#include "Item.h"

int main()
{

  char option;
  std::string str= " ";
  std::string NA_ME = " ";
  std::string  PR_ICE = " ";
  std::string ST_OCK = " ";

  Item IT;

  Dvd dvd = Dvd();
  Cd cd = Cd();
  Magazine mag = Magazine();
  Book book = Book();



  Item * DVD = &dvd;
  Item * CD = &cd;
  Item * MAGAZINE = &mag;
  Item * BOOK = &book;



  do {
    std::cout << "\n******** Stock Management System ********\n";
    std::cout << "\n******** Menu ********\n";
    std::cout << "S : Sell items \n";
    std::cout << "R : Restock items \n";
    std::cout << "A : Add new items \n";
    std::cout << "U : Update stock quantity \n";
    std::cout << "V : View report of sales \n";
    std::cout << "1 : Load stock data from file \n";
    std::cout << "Q : quit\n";

    std::cout << "\n\n Select an option from above: ";
    std::cin >> option;

    if (option == 'S' || option == 's') {
        std::cout << " \n************************************\n You choose 'S' : Sell items \n************************************\n";
        std::cout<<"\nEnter the item's category you would like to sell: \n 1.) DVD\n 2.) CD\n 3.) MAGAZINE\n 4.) BOOK\n-----------------\n\n";
        std::cin >> str;
        std::cout<<"\n-----------------\n";
        if(str == "DVD")
        {
            std::cout<<"Enter the name of the item: \n";
            std::cin.ignore();
            std::getline(std::cin, NA_ME);
            DVD->SetName(NA_ME);
            std::cout<<"Enter the quantity of the item to be sold: \n";
            std::cin >> ST_OCK;
            DVD->SetStock(ST_OCK);
            std::cout<<"Enter the price of the item: \n";
            std::cin >> PR_ICE;
            DVD->SetPrice(PR_ICE);
            dvd.sell_Item(&dvd);

        }else if (str == "CD")
        {
            std::cout<<"Enter the name of the item: \n";
            std::cin.ignore();
            std::getline(std::cin, NA_ME);
            CD->SetName(NA_ME);
            std::cout<<"Enter the quantity of the item to be sold: \n";
            std::cin >> ST_OCK;
            CD->SetStock(ST_OCK);
            std::cout<<"Enter the price of the item: \n";
            std::cin >> PR_ICE;
            CD->SetPrice(PR_ICE);
            cd.sell_Item(&cd);

        }else if (str == "MAGAZINE")
        {
            std::cout<<"Enter the name of the item: \n";
            std::cin.ignore();
            std::getline(std::cin, NA_ME);
            MAGAZINE->SetName(NA_ME);
            std::cout<<"Enter the quantity of the item to be sold: \n";
            std::cin >> ST_OCK;
            MAGAZINE->SetStock(ST_OCK);
            std::cout<<"Enter the price of the item: \n";
            std::cin >> PR_ICE;
            MAGAZINE->SetPrice(PR_ICE);
            mag.sell_Item(&mag);

        }else if (str == "BOOK")
        {
            std::cout<<"Enter the name of the item: \n";
            std::cin.ignore();
            std::getline(std::cin, NA_ME);
            BOOK->SetName(NA_ME);
            std::cout<<"Enter the quantity of the item to be sold: \n";
            std::cin >> ST_OCK;
            BOOK->SetStock(ST_OCK);
            std::cout<<"Enter the price of the item: \n";
            std::cin >> PR_ICE;
            BOOK->SetPrice(PR_ICE);
            book.sell_Item(&book);

        }else
        {
            std::cout << " \n Invalid option. Please enter one of these options DVD, CD, MAGAZINE, BOOK \n ";
        }


    } else if (option == 'R' || option == 'r') {
        std::cout << " \n************************************\n You choose 'R' : Restock items \n************************************\n";
        std::cout<<"\nEnter the item's category you would like to restock: \n 1.) DVD\n 2.) CD\n 3.) MAGAZINE\n 4.) BOOK\n-----------------\n\n";
        std::cin >> str;
        std::cout<<"\n-----------------\n";
        if(str == "DVD")
        {
            std::cout<<"Enter the name of the item: \n";
            std::cin.ignore();
            std::getline(std::cin, NA_ME);
            DVD->SetName(NA_ME);
            std::cout<<"Enter the stock of the item: \n";
            std::cin >> ST_OCK;
            DVD->SetStock(ST_OCK);
            dvd.restock_Item(&dvd);

        }else if (str == "CD")
        {
            std::cout<<"Enter the name of the item: \n";
            std::cin.ignore();
            std::getline(std::cin, NA_ME);
            CD->SetName(NA_ME);
            std::cout<<"Enter the stock of the item: \n";
            std::cin >> ST_OCK;
            CD->SetStock(ST_OCK);
            cd.restock_Item(&cd);

        }else if (str == "MAGAZINE")
        {
            std::cout<<"Enter the name of the item: \n";
            std::cin.ignore();
            std::getline(std::cin, NA_ME);
            MAGAZINE->SetName(NA_ME);
            std::cout<<"Enter the stock of the item: \n";
            std::cin >> ST_OCK;
            MAGAZINE->SetStock(ST_OCK);
            mag.restock_Item(&mag);

        }else if (str == "BOOK")
        {
            std::cout<<"Enter the name of the item: \n";
            std::cin.ignore();
            std::getline(std::cin, NA_ME);
            BOOK->SetName(NA_ME);
            std::cout<<"Enter the stock of the item: \n";
            std::cin >> ST_OCK;
            BOOK->SetStock(ST_OCK);
            book.restock_Item(&book);

        }else
        {
            std::cout << " \n Invalid option. Please enter one of these options DVD, CD, MAGAZINE, BOOK \n ";
        }


    } else if (option == 'A' || option == 'a') {
        std::cout << " \n************************************\n You choose 'A' : Add new items \n************************************\n";
        std::cout<<"\nEnter the item's category you would like to add: \n 1.) DVD\n 2.) CD\n 3.) MAGAZINE\n 4.) BOOK\n-----------------\n\n";
        std::cin >> str;
        std::cout<<"\n-----------------\n";
         if(str == "DVD")
        {
            std::cout<<"Enter the name of the item: \n";
            std::cin.ignore();
            std::getline(std::cin, NA_ME);
            DVD->SetName(NA_ME);
            std::cout<<"Enter the quantity of the item to be added: \n";
            std::cin >> ST_OCK;
            DVD->SetStock(ST_OCK);
            std::cout<<"Enter the price of the item: \n";
            std::cin >> PR_ICE;
            DVD->SetPrice(PR_ICE);
            dvd.add_Item(&dvd);

        }else if (str == "CD")
        {
            std::cout<<"Enter the name of the item: \n";
            std::cin.ignore();
            std::getline(std::cin, NA_ME);
            CD->SetName(NA_ME);
            std::cout<<"Enter the quantity of the item to be added: \n";
            std::cin >> ST_OCK;
            CD->SetStock(ST_OCK);
            std::cout<<"Enter the price of the item: \n";
            std::cin >> PR_ICE;
            CD->SetPrice(PR_ICE);
            cd.add_Item(&cd);

        }else if (str == "MAGAZINE")
        {
            std::cout<<"Enter the name of the item: \n";
            std::cin.ignore();
            std::getline(std::cin, NA_ME);
            MAGAZINE->SetName(NA_ME);
            std::cout<<"Enter the quantity of the item to be added: \n";
            std::cin >> ST_OCK;
            MAGAZINE->SetStock(ST_OCK);
            std::cout<<"Enter the price of the item: \n";
            std::cin >> PR_ICE;
            MAGAZINE->SetPrice(PR_ICE);
            mag.add_Item(&mag);

        }else if (str == "BOOK")
        {
            std::cout<<"Enter the name of the item: \n";
            std::cin.ignore();
            std::getline(std::cin, NA_ME);
            BOOK->SetName(NA_ME);
            std::cout<<"Enter the quantity of the item to be added: \n";
            std::cin >> ST_OCK;
            BOOK->SetStock(ST_OCK);
            std::cout<<"Enter the price of the item: \n";
            std::cin >> PR_ICE;
            BOOK->SetPrice(PR_ICE);
            book.add_Item(&book);

        }else
        {
            std::cout << " \n Invalid option. Please enter one of these options DVD, CD, MAGAZINE, BOOK \n ";
        }


    } else if (option == 'U' || option == 'u') {
        std::cout << " \n************************************\n You choose 'U' : saved data to file / Update stock quantity \n************************************\n";
        std::cout<<"\nEnter the item's category you would like to update stocks of: \n 1.) DVD\n 2.) CD\n 3.) MAGAZINE\n 4.) BOOK\n-----------------\n\n";
        std::cin >> str;
        std::cout<<"\n-----------------\n";
        if(str == "DVD")
        {
              IT.save_update_Stockdata_tofile(&dvd);

        }else if (str == "CD")
        {
              IT.save_update_Stockdata_tofile(&cd);

        }else if (str == "MAGAZINE")
        {
              IT.save_update_Stockdata_tofile(&mag);

        }else if (str == "BOOK")
        {
              IT.save_update_Stockdata_tofile(&book);

        }else
        {
            std::cout << " \n Invalid option. Please enter one of these options DVD, CD, MAGAZINE, BOOK \n ";
       }

        std::cout << " \n Done! Updated DVD, CD, MAGAZINE, BOOK stocks to file \n ";

    } else if (option == 'V' || option == 'v') {
      std::cout << " \n************************************\n You choose 'V' : Viewing Sales Report \n************************************\n";
      std::cout<<"\nEnter the item category you would like to view: \n 1.) DVD\n 2.) CD\n 3.) MAGAZINE\n 4.) BOOK\n-----------------\n\n";
      std::cin >> str;
      std::cout<<"\n-----------------\n";
      if(str == "DVD")
        {
            dvd.view_sales_Report(&dvd);

        }else if (str == "CD")
        {
            cd.view_sales_Report(&cd);

        }else if (str == "MAGAZINE")
        {
            mag.view_sales_Report(&mag);

        }else if (str == "BOOK")
        {
            book.view_sales_Report(&book);

        }else
        {
            std::cout << " \n Invalid option. Please enter one of these options DVD, CD, MAGAZINE, BOOK \n ";
        }

    } else if (option == '1') {
      std::cout << " \n************************************\n You choose '1' : Loading stock data from file \n************************************\n";
      IT.load_data_from_file();   

    } else if (option != 'Q' && option != 'q') {
      std::cout << " \n Invalid option. Please enter S,R,A,U,V,1 or Q \n ";
    }

  } while (option != 'Q' && option != 'q');

  std::cout << "\n\t\tThank You!\n";

  return 0;
}