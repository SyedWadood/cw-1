#ifndef _ITEM_H_
#define _ITEM_H_

/*
Item.h
Author: M00734132
Created: 12/01/2021
Updated: 22/01/2021
*/

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include<iterator>


class Item
{
    friend class Book;
    friend class Magazine;
    friend class Dvd;
    friend class Cd;

    private:
        std::string Name; 
        std::string Price; 
        std::string Stock; 
        std::string LineRead_str;
        std::vector <Item*> Product_Vec;
        std::vector <Item*> temp1;
        std::vector <Item*> Sold_Product_Vec;
        std::vector <Item*> temp2;


    public:
        Item();/** Default constructor */
        Item(std::string Name, std::string Price, std::string Stock);

        std::string GetName();
        void SetName(std::string Name);
        std::string GetPrice();
        void SetPrice(std::string Price);
        std::string GetStock();
        void SetStock(std::string Stock);

        void sell_Item(Item * ITEM);  // Sell items fucntion
        void restock_Item(Item * ITEM); // Restock items fucntion
        void add_Item(Item * ITEM); // Add items fucntion
        void save_update_Stockdata_tofile(Item * ITEM); // Update & save to file fucntion
        void view_sales_Report(Item * ITEM); // View report of sales fucntion
        void load_data_from_file(); // Load data to file fucntion

};

class Book : public Item
{
    private:

    public:
        Book();/** Default constructor */
        Book(std::string Name, std::string Price, std::string Stock);



};

class Magazine : public Item
{
    private:

    public:
        Magazine();/** Default constructor */
        Magazine(std::string Name, std::string Price, std::string Stock);


};

class Dvd : public Item
{
   private:

   public:
        Dvd();/** Default constructor */
        Dvd(std::string Name, std::string Price, std::string Stock);


};

class Cd : public Item
{
   private:

   public:
        Cd();/** Default constructor */
        Cd(std::string Name, std::string Price, std::string Stock);


};

#endif
