/*
Item.cpp
Author: M00734132
Created: 12/01/2021
Updated: 22/01/2021
*/

#include "Item.h"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include<iterator>


Item::Item()
{

}

Item::Item(std::string Name, std::string Price, std::string Stock)
{
    this->Name = Name;
    this->Price = Price;
    this->Stock = Stock;
}

std::string Item :: GetName()
{
    return Name;
}
/** Set Name
* \param Name New value to set
*/
void Item :: SetName(std::string Name)
{
    this->Name = Name;
}

/** Access Price
* \return The current value of Price
*/
std::string Item :: GetPrice()
{
    return Price;
}
/** Set Price
* \param Price New value to set
*/
void Item :: SetPrice(std::string Price)
{
    this->Price = Price;
}

/** Access Stock
* \return The current value of Stock
*/
std::string Item :: GetStock()
{
    return Stock;
}
/** Set Stock
* \param Stock New value to set
*/
void Item :: SetStock(std::string Stock)
{
    this->Stock = Stock;
}


void Item::sell_Item(Item * ITEM) // Sell items fucntion
{
this->Sold_Product_Vec.clear();
this->Sold_Product_Vec.push_back(ITEM);
this->Sold_Product_Vec.swap(temp2);
this->temp2.clear();
}  

void Item::restock_Item(Item * ITEM) // Restock items fucntion
{
this->Product_Vec.clear();
this->Product_Vec.push_back(ITEM);
this->Product_Vec.swap(temp1);
this->temp1.clear();
}

void Item::add_Item(Item * ITEM) // Add items fucntion
{
this->Product_Vec.clear();
this->Product_Vec.push_back(ITEM);
this->Product_Vec.swap(temp1);
this->temp1.clear();
}

void Item::save_update_Stockdata_tofile(Item * ITEM) // Update & save to file fucntion
{
std::cout<<"In Update Stock Function \n";
this->Product_Vec.push_back(ITEM);
this->Product_Vec.swap(temp1);

std::fstream output_file;
output_file.open("UpdatedStocksFile.txt",std::fstream::in | std::fstream::out | std::fstream::app);

if (!output_file.fail() && output_file.is_open()){

  for (Item * i : this->temp1)
  output_file << i->GetName() << "\t\t\t\t\n" << i->GetStock() << "\t\t\t\t\n" <<  i->GetPrice() << "\n" << std::endl;

  for (Item * i : this->temp1)
  std::cout << i->GetName() << "\t\t\t\t\n" << i->GetStock() << "\t\t\t\t\n" <<  i->GetPrice() << "\n" << std::endl;

  this->temp1.clear();
  output_file.close();

}else{
  std::cout << " failed to open !"<< std::endl;
}

}
void Item::view_sales_Report(Item * ITEM) // View report of sales fucntion
{
  this->Sold_Product_Vec.push_back(ITEM);
  this->Sold_Product_Vec.swap(temp2);

  std::cout << "The item's name is: \n";
  for (Item * i : this->temp2)
  std::cout << i->GetName() << std::endl;

  std::cout << "The number of item's sold are: \n";
  for (Item * i : this->temp2)
  std::cout << i->GetStock() << std::endl;

  std::cout << "Price sold at: \n";
  for (Item * i : this->temp2)
  std::cout << i->GetPrice() << "$" << std::endl;

  this->temp2.clear();
} 

void Item::load_data_from_file() // Load data to file fucntion
{ 
  std::cout << "File Contents consists of name, stock, price of the items respectively:- \n\n";
  std::ifstream input_file;
  input_file.open ("UpdatedStocksFile.txt", std::ifstream::in);
  if (!input_file.fail() && input_file.is_open()){
    while(getline(input_file,LineRead_str))
    { 
        std::cout << LineRead_str << "\n";
    }
  input_file.close();

}else{
  std::cout << " failed to open !"<< std::endl;
}
}

Book::Book() /** Default constructor */
{

}

Book::Book(std::string Name, std::string Price, std::string Stock)
{
    this->Price = Price;
    this->Stock = Stock;
}


Magazine::Magazine() /** Default constructor */
{

}

Magazine::Magazine(std::string Name, std::string Price, std::string Stock)
{
    this->Price = Price;
    this->Stock = Stock;
}


Dvd::Dvd() /** Default constructor */
{

}

Dvd::Dvd(std::string Name, std::string Price, std::string Stock)
{
    this->Price = Price;
    this->Stock = Stock;
}


Cd::Cd() /** Default constructor */
{

}

Cd::Cd(std::string Name, std::string Price, std::string Stock)
{
    this->Price = Price;
    this->Stock = Stock;
}